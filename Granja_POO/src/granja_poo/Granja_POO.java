
package granja_poo;

import entidades.Animal;
import entidades.Establo;
import java.util.Scanner;
import vista.Principal;

public class Granja_POO {

    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       int opcion = 0;
       
       
       Principal ventanaPrincipal = new Principal();
       ventanaPrincipal.setVisible(true);
       
       
       Establo establos[] = new Establo[3];
       //Inicializar los 3 establos sin el menu.       
       String menu = "=======MENU=======";//Agregar establo al menu
              menu+= "\n1.-Agregar Establo";
              menu+= "\n2.-Ingresar animal";
              menu+= "\n3.-Vacunar Animales (dentro de un establo)";
              menu+= "\n4.-Mostrar Animales (dentro de un establo)";
              menu+= "\n5.-Salir";
       
        while(opcion != 5){
            System.out.println(menu);
            opcion= sc.nextInt();
            
            switch(opcion){
                case 1:
                    for(int i=0; i<establos.length; i++){
                        if(establos[i] == null){
                            System.out.println("Elija un tamaño para su establo");
                            System.out.println("1.-Grande [20]");
                            System.out.println("2.-Mediano [15]");
                            System.out.println("3.-Chico [10]");
                            int resp = sc.nextInt();
                            String tamanio = "grande";
                            //if ternario                            
                            //( condicion ? true : false )                            
                            if(resp == 2){
                                tamanio = "mediano";
                            }else if(resp == 3){
                                tamanio = "chico";
                            }                            
                            establos[i] = new Establo(tamanio);
                            System.out.println("Defina el color de su establo");
                            establos[i].setColor(sc.next());
                            System.out.println("Se ha agregado correctamente el establo");
                            break;
                        }
                        
                        if((i+1) == establos.length){
                            System.out.println("Ya no puede agregar mas establos.");
                        }
                        
                    }
                    
                    break; //Agregar Establo
                case 2: 
                    
                    System.out.println("Ingrese el establo");
                    for(int i=0; i<establos.length; i++){
                        if(establos[i] != null){
                            System.out.println("["+(i+1)+"] "+establos[i].getColor());
                        }
                    }
                    int establo_sel = sc.nextInt()-1;
                    
                    System.out.println("=== Seleccione una opcion ===");
                    System.out.println("1.-Agregar un nuevo animal");
                    for(int i=0; i<establos[establo_sel].getAnimales().length; i++ ){
                        if(establos[establo_sel].getAnimales()[i] != null){
                            String menu_animal = (i+2)+".-";
                                   menu_animal+= establos[establo_sel].getAnimales()[i].getTipo();
                                   menu_animal+= " "+establos[establo_sel].getAnimales()[i].getCantidad() +"/"+establos[establo_sel].getTamanio();
                            System.out.println(menu_animal);
                        }
                    }
                    int op_sel = sc.nextInt();
                    
                    if(op_sel == 1){
                        Animal a = new Animal();
                        System.out.println("Digite el tipo de animal:");
                        a.setTipo(sc.next());
                        establos[establo_sel].agregarTipoAnimal(a);
                        
                    }else{//Asignar uno a la cantidad
                        op_sel = op_sel-2;
                        establos[establo_sel].agregarAnimalExistente(op_sel);
                    }
                    
                    
                    break; //Ingresar animales
                case 3:
                    System.out.println("Ingrese el establo");
                    for(int i=0; i<establos.length; i++){
                        if(establos[i] != null){
                            System.out.println("["+(i+1)+"] "+establos[i].getColor());
                        }
                    }
                    int establo_selV = sc.nextInt()-1;
                    
                    System.out.println("=== Animales ===");
                    for(int i=0; i<establos[establo_selV].getAnimales().length; i++ ){
                        if(establos[establo_selV].getAnimales()[i] != null){
                            String menu_animal = (i+1)+".-";
                                   menu_animal+= establos[establo_selV].getAnimales()[i].getTipo();
                            System.out.println(menu_animal);
                        }
                    }
                    int op_selV = sc.nextInt()-1;
                    
                    establos[establo_selV].vacunar(op_selV);
                    System.out.println("Se ha vacunado a todos los(las) "+establos[establo_selV].getAnimales()[op_selV].getTipo());
                    
                    break; //Vacunar animales
                case 4: 
                    System.out.println("Ingrese el establo");
                    for(int i=0; i<establos.length; i++){
                        if(establos[i] != null){
                            System.out.println("["+(i+1)+"] "+establos[i].getColor());
                        }
                    }
                    int establo_selMostrar = sc.nextInt()-1;
                    
                    System.out.println("=== Animales ===");
                    for(int i=0; i<establos[establo_selMostrar].getAnimales().length; i++ ){
                        if(establos[establo_selMostrar].getAnimales()[i] != null){
                            Animal animalMostrar = establos[establo_selMostrar].getAnimales()[i];
                            String menu_animal = animalMostrar.getTipo()
                                    +( animalMostrar.isVacuna() ? " [Vacunado] " : " [Sin vacunas] ")
                                    +"  "+animalMostrar.getCantidad()+"/"+establos[establo_selMostrar].getTamanio();
                            System.out.println(menu_animal);
                        }
                    }
                    
                    break; //Mostrar animales
                case 5:
                    System.out.println("Gracias por utilizar nuestro sistema..");
                    System.exit(0);
                    break; //Salir
                
                default: System.out.println("Opcion no valida"); break;
            }
            
        }      
              
              
    }
    
}
