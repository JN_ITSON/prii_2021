package entidades;

public class Establo {
    private String color = new String();
    private String tamanio = new String();//chico[10][5] - mediano[15][7] - grande[20][15]
    private Animal animales[];
    
    
    public Establo(String tamanio){
        this.tamanio = tamanio;
        
        if(this.tamanio.toLowerCase().equals("chico")){
            this.animales = new Animal[10];
        }else if(this.tamanio.toLowerCase().equals("mediano")){
            this.animales = new Animal[15];
        }else{//grande
            this.animales = new Animal[20];
        }
        
    }
    
    public String getColor(){
        return this.color;
    }
    
    public void setColor(String color){
        this.color = color;
    }
    
    public Animal[] getAnimales(){
        return this.animales;
    }
    
    public int getTamanio(){
        int tamanio_maximo = 15;//chico[10][5] - mediano[15][7] - grande[20][15]
        if(this.tamanio.equals("chico")){
            tamanio_maximo = 5;
        }else if(this.tamanio.equals("mediano")){
            tamanio_maximo = 7;
        }
       return tamanio_maximo;
    }
    
    
    public void agregarTipoAnimal(Animal animal){//nuevo tipo de animal
        for(int i=0; i<this.animales.length; i++){
            if(this.animales[i] == null){
//                animal.setCantidad(1);
                this.animales[i] = animal;
                System.out.println("Se agregó el tipo de animal correctamente.");
                break;
            }
            
            if(this.animales.length == (i+1)){
                System.out.println("No hay espacio en el establo para nuevos animales.");
            }
            
        }
    }
    
    public void agregarAnimalExistente(int pos){// aumento cantidad
        int tamanio_maximo = 15;//chico[10][5] - mediano[15][7] - grande[20][15]
        if(this.tamanio.equals("chico")){
            tamanio_maximo = 5;
        }else if(this.tamanio.equals("mediano")){
            tamanio_maximo = 7;
        }
       
        int cantidad_actual = this.animales[pos].getCantidad();
        if(tamanio_maximo > cantidad_actual){
            cantidad_actual++;
            this.animales[pos].setCantidad(cantidad_actual);
            System.out.println("Se ingresó correctamente el animal al establo.");
        }else{
            System.out.println("Ya no hay espacio en el establo para este tipo de animal.");
        }
        
    }
    
    public void vacunar(int pos){
       this.animales[pos].setVacuna(true);
    }
    
}
