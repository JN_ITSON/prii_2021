package entidades;

public class Animal {
    private String tipo = new String();
    private int cantidad = 0; //Capacidad en el establo seleccionado
    private boolean vacuna = false;

    public Animal() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public boolean isVacuna() {
        return vacuna;
    }

    public void setVacuna(boolean vacuna) {
        this.vacuna = vacuna;
    }
    
    
    
    
}
