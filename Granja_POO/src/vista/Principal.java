/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import entidades.Animal;
import entidades.Establo;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jorge
 */
public class Principal extends javax.swing.JFrame {

    
    public static Establo establos[] = new Establo[3];
    int indiceAnimal = -1;
    int establoSel = -1;
    
    
    /**
     * Creates new form Principal
     */
    public Principal() {
        initComponents();
        
        this.establos[0] = new Establo("grande");
        this.establos[0].setColor("Rojo");
        
        this.establos[1] = new Establo("mediano");
        this.establos[1].setColor("Amarillo");
        
        
        this.establos[2] = new Establo("chico");
        this.establos[2].setColor("Verde");
        
        
        llenarTablas();
    }
    
    
    
    public void llenarTablas(){
        
        DefaultTableModel modelo = (DefaultTableModel) tblEstabloUno.getModel();
        DefaultTableModel modeloDos = (DefaultTableModel) tblEstabloDos.getModel();
        DefaultTableModel modeloTres = (DefaultTableModel) tblEstabloTres.getModel();
        
        
        modelo.setRowCount(0);//Resetear las filas de la tabla o modelo
        modeloDos.setRowCount(0);
        modeloTres.setRowCount(0);
        
        
        //Llenamos la tabla del primer establo
        if(this.establos[0].getAnimales() != null){
            for(Animal animal :this.establos[0].getAnimales() ){
                if(animal != null){
                    modelo.addRow(new Object[]{
                                    animal.getTipo(),
                                    animal.getCantidad(),
                                    (animal.isVacuna() ? "SI" :" NO ")
                    });
                }else{
                    modelo.addRow(new Object[]{"",0,"NO"});
                }
            }
        }
        
        //Llenado del segundo establo
        if(this.establos[1].getAnimales() != null){
            for(Animal animal :this.establos[1].getAnimales() ){
                if(animal != null){
                    modeloDos.addRow(new Object[]{
                                    animal.getTipo(),
                                    animal.getCantidad(),
                                    (animal.isVacuna() ? "SI" :" NO ")
                    });
                }else{
                    modeloDos.addRow(new Object[]{"",0,"NO"});
                }
            }
        }
        
        //Llenado del segundo establo
        if(this.establos[2].getAnimales() != null){
            for(Animal animal :this.establos[2].getAnimales() ){
                if(animal != null){
                    modeloTres.addRow(new Object[]{
                                    animal.getTipo(),
                                    animal.getCantidad(),
                                    (animal.isVacuna() ? "SI" :" NO ")
                    });
                }else{
                    modeloTres.addRow(new Object[]{"",0,"NO"});
                }
            }
        }
        
        
        tblEstabloUno.setModel(modelo);//Devolvemos el modelo editado a la tabla
        tblEstabloDos.setModel(modeloDos);
        tblEstabloTres.setModel(modeloTres);
        
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        btnAgregar = new javax.swing.JButton();
        btnVacunar = new javax.swing.JButton();
        btnRefrescar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        tbdEstablo = new javax.swing.JTabbedPane();
        panelEstablo1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblEstabloUno = new javax.swing.JTable();
        panelEstablo2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblEstabloDos = new javax.swing.JTable();
        panelEstablo3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblEstabloTres = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(102, 102, 102));

        jToolBar1.setRollover(true);

        btnAgregar.setText("Agregar");
        btnAgregar.setFocusable(false);
        btnAgregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAgregar);

        btnVacunar.setText("Vacunar");
        btnVacunar.setFocusable(false);
        btnVacunar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnVacunar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnVacunar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVacunarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnVacunar);

        btnRefrescar.setText("Refrescar");
        btnRefrescar.setFocusable(false);
        btnRefrescar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefrescar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnRefrescar);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("SISTEMA DE AMINISTRACIÓN DE ESTABLOS");

        tblEstabloUno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TIPO", "CANTIDAD", "VACUNADO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblEstabloUno.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblEstabloUno.getTableHeader().setReorderingAllowed(false);
        tblEstabloUno.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEstabloUnoMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tblEstabloUno);
        tblEstabloUno.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        javax.swing.GroupLayout panelEstablo1Layout = new javax.swing.GroupLayout(panelEstablo1);
        panelEstablo1.setLayout(panelEstablo1Layout);
        panelEstablo1Layout.setHorizontalGroup(
            panelEstablo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 871, Short.MAX_VALUE)
        );
        panelEstablo1Layout.setVerticalGroup(
            panelEstablo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
        );

        tbdEstablo.addTab("ESTABLO1", panelEstablo1);

        tblEstabloDos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TIPO", "CANTIDAD", "VACUNADO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblEstabloDos.setColumnSelectionAllowed(true);
        tblEstabloDos.getTableHeader().setReorderingAllowed(false);
        tblEstabloDos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEstabloDosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblEstabloDos);
        tblEstabloDos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        javax.swing.GroupLayout panelEstablo2Layout = new javax.swing.GroupLayout(panelEstablo2);
        panelEstablo2.setLayout(panelEstablo2Layout);
        panelEstablo2Layout.setHorizontalGroup(
            panelEstablo2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 871, Short.MAX_VALUE)
        );
        panelEstablo2Layout.setVerticalGroup(
            panelEstablo2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
        );

        tbdEstablo.addTab("ESTABLO2", panelEstablo2);

        tblEstabloTres.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TIPO", "CANTIDAD", "VACUNADO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblEstabloTres.setColumnSelectionAllowed(true);
        tblEstabloTres.getTableHeader().setReorderingAllowed(false);
        tblEstabloTres.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEstabloTresMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblEstabloTres);
        tblEstabloTres.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        javax.swing.GroupLayout panelEstablo3Layout = new javax.swing.GroupLayout(panelEstablo3);
        panelEstablo3.setLayout(panelEstablo3Layout);
        panelEstablo3Layout.setHorizontalGroup(
            panelEstablo3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 871, Short.MAX_VALUE)
        );
        panelEstablo3Layout.setVerticalGroup(
            panelEstablo3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
        );

        tbdEstablo.addTab("ESTABLO3", panelEstablo3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(tbdEstablo)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tbdEstablo, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
       
        AnimalForm animalForm = new AnimalForm();
        animalForm.setVisible(true);
        llenarTablas();
        
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnVacunarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVacunarActionPerformed
        if(this.establoSel > -1 && this.indiceAnimal > -1){
            if(this.establos[this.establoSel].getAnimales()[this.indiceAnimal] != null){
                this.establos[this.establoSel].vacunar(this.indiceAnimal);
                llenarTablas();
                JOptionPane.showMessageDialog(this, "Se ha vacunado correctamente.");
            }else{
                JOptionPane.showMessageDialog(this, "No se ha vacunado, porque no existe.");
            }
        }else{
            JOptionPane.showMessageDialog(this, "No se ha seleccionado un animal.");
        }
        this.establoSel = -1;
        this.indiceAnimal = -1;
    }//GEN-LAST:event_btnVacunarActionPerformed

    private void btnRefrescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarActionPerformed
        // TODO add your handling code here:
        llenarTablas();
    }//GEN-LAST:event_btnRefrescarActionPerformed

    private void tblEstabloUnoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEstabloUnoMouseClicked
        this.establoSel = 0;
        this.indiceAnimal =  tblEstabloUno.getSelectedRow();         
        
    }//GEN-LAST:event_tblEstabloUnoMouseClicked

    private void tblEstabloDosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEstabloDosMouseClicked
        this.establoSel = 1;
        this.indiceAnimal =  tblEstabloDos.getSelectedRow();   
    }//GEN-LAST:event_tblEstabloDosMouseClicked

    private void tblEstabloTresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEstabloTresMouseClicked
        this.establoSel = 2;
        this.indiceAnimal =  tblEstabloTres.getSelectedRow(); 
    }//GEN-LAST:event_tblEstabloTresMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnRefrescar;
    private javax.swing.JButton btnVacunar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JPanel panelEstablo1;
    private javax.swing.JPanel panelEstablo2;
    private javax.swing.JPanel panelEstablo3;
    private javax.swing.JTabbedPane tbdEstablo;
    private javax.swing.JTable tblEstabloDos;
    private javax.swing.JTable tblEstabloTres;
    private javax.swing.JTable tblEstabloUno;
    // End of variables declaration//GEN-END:variables
}
