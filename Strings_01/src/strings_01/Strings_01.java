/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strings_01;

/**
 *
 * @author jorge
 */
public class Strings_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        String s1 = "Anita lava la tina";
        String s2 = "anitA lAvA lA Tina";
        
        
        System.out.println("Length: "+ s1.length() );
        
        //indexOf (caracter)  devuelve la posición
        System.out.println("La primer posición de 'l' es: "+s1.indexOf("l"));
        System.out.println("La posición de 's' es: "+ s1.indexOf("s"));
        
        //lastIndexOf(caracter devuelve la ultima posición
        System.out.println("La ultima posición de 'l' es: "+s1.lastIndexOf("l"));
        System.out.println("La posición de 's' es: "+ s1.lastIndexOf("s"));
        
        //charAt(n) 
        System.out.println("El carácter de s1 en la posición 7 es:"+s1.charAt(7));
        
        //substring(PrimerIndicie,UltimoIndice-1) 
        String s3 = s1.substring(6, 10);
        System.out.println("El valor de s3: "+s3);
        
        
        //toUpperCase  convierte todo Mayusculas
        System.out.println(s1.toUpperCase());
        
        //toLowerCase  convierte todo a minusculas
        System.out.println(s1.toLowerCase());
        
        
        //Equals    ==   equals
        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));
        
        //equalsIgnoreCase
        System.out.println(s1.toLowerCase() == s2.toLowerCase());
        System.out.println(s1.equalsIgnoreCase(s2));
        
        //compareTo
        String s4 = "A";
        String s5 = "a";
        String s6 = "A";
        System.out.println("A comparado con a: "+s4.compareTo(s5));
        System.out.println("a comparado con A: "+s5.compareTo(s4));
        System.out.println("A comparado con A: "+s4.compareTo(s6));
        
        //compareToIgnoreCase
        System.out.println("A comparado con a: "+s4.compareToIgnoreCase(s5));
        
        //Si el string está vacio isEmpty
        System.out.println("Está vacio? "+s4.isEmpty());
        String s7="";
        System.out.println("Está vacio? "+s7.isEmpty());
        
        String s8 = new String("String de prueba");
        
        
        //valueOf 
        
        System.out.println(1);
        int numero = 1;
        String s9= String.valueOf(numero);
        System.out.println(s9);
        
        //Split
        
        String s10[] = s1.split(" ");// :  ; |
        String user = "jorge:jorge@gmail.com:15:Ing. En software";
        for(String valor: s10){
            System.out.println(valor);
        }
        
        String usuario[] = user.split(":");
        System.out.println("Nombre: "+usuario[0]);
        System.out.println("Carrera: "+usuario[3]);
        
    }
    
}
