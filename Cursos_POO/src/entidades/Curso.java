
package entidades;

public class Curso {

    /*
        La práctica hace al maestro.
    */
    
    private String nombre= new String();
    private String descripcion = new String();
    private String fechaInicio = new String();
    private String fechaFin = new String();
    private String horario = new String();
    
    private Alumno alumnos[] = new Alumno[20];
    
    //Constructores
    public Curso(){}
    //Getters and Setters

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Alumno[] getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(Alumno al[]){
        this.alumnos = al;
    }
    
    
    //Metodos de la clase
    public void agregarAlumno(Alumno alumno){
        for(int i=0; i<this.alumnos.length; i++){
            if(this.alumnos[i] == null){
                this.alumnos[i] = alumno;
                System.out.println("El alumno se registró correctamente");
                break;
            }
            
            if((i+1) == this.alumnos.length){
                System.out.println("La lista del curso está completa.");
            }
            
        }
    }
    
    public void eliminarAlumno(int ialumno){
        this.alumnos[ialumno] = null;
        System.out.println("Se ha eliminado el alumno correctamente.");
        
    }
    
    
    public static String metodoDentroDeCurso(){
        return "Hola soy un metodo static dentro de curso";
    }
    
}
