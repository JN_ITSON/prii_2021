package cursos_poo;

import entidades.Alumno;
import entidades.Curso;
import java.util.Scanner;

public class Cursos_POO {
    
    private static int opcion = 0; //Variable con un scope global (alcance global)
    
    public static void main(String[] args) {//Variable local que se inicializa por parametro.
        //Declaracion de variables
        Scanner sc = new Scanner(System.in);
        Curso cursos[] = new Curso[5];
        int opcion = 0; //Variable local
        
        int icurso = 0;
        String menu = "----[Menú]----";
               menu+= "\n1.-Agregar Curso";
               menu+= "\n2.-Agregar un Alumno a un curso";
               menu+= "\n3.-Dar de baja un alumno";               
               menu+= "\n4.-Mostrar los cursos";
               menu+= "\n5.-Mostrar Lista de Alumnos";
               menu+= "\n6.-Salir";
               menu+= "\nDigite su opción: ";
      
        do{
            System.out.println(menu);
            opcion = sc.nextInt();
            
            switch(opcion){
                case 1:
                    
                    for(int i=0; i<cursos.length; i++){
                        if(cursos[i] == null){
                            cursos[i] = new Curso();
                            System.out.println("Digite el nombre del curso");
                            cursos[i].setNombre(sc.next());
                            System.out.println("Digite la descripcion del curso");
                            cursos[i].setDescripcion(sc.next());
                            System.out.println("Digite la fecha de inicio");
                            cursos[i].setFechaInicio(sc.next());
                            System.out.println("Digite la fecha de finalización");
                            cursos[i].setFechaFin(sc.next());
                            System.out.println("Digite el horario del curso");
                            cursos[i].setHorario(sc.next());
                            
                            System.out.println("El curso se ha agregado correctamente. Precione enter para continuar");
                            sc.next();
                            
                            break;
                        }
                        
                        if((i+1) == cursos.length){
                            System.out.println("No se pueden agregar mas cursos");
                        }
                        
                    }
                    
                    
                    break;
                case 2:
                    System.out.println("Seleccione un curso por su [indice]: ");
                    imprimirCursos(cursos);
                    icurso = sc.nextInt() - 1;
                    
                    Alumno alumno = new Alumno();
                    System.out.println("Digite el nombre del alumno: ");
                    alumno.setNombre(sc.next());
                   
                    cursos[icurso].agregarAlumno(alumno);                    
                    
                    break;
                case 3:
                    System.out.println("Seleccione un curso por su [indice]: ");
                    imprimirCursos(cursos);
                    icurso = sc.nextInt() - 1;
                    
                    System.out.println("Seleccione un alumno por su [indice]: ");
                    mostrarListaAlumnos(cursos[icurso].getAlumnos());
                    int ialumno = sc.nextInt() - 1;
                    cursos[icurso].eliminarAlumno(ialumno);                    
                    
                    break;
                case 4:
                    for(Curso curso : cursos){
                        if(curso != null){
                            System.out.println("=======================");

                            System.out.println("Nombre: "+curso.getNombre());
                            System.out.println("Descripcion: "+curso.getDescripcion());
                            System.out.println("Fecha: "+curso.getFechaInicio()+" - "+curso.getFechaFin());
                            System.out.println("Horario: "+curso.getHorario());

                            System.out.println("=======================");
                        }
                    }                    
                    break;
                case 5:
                    System.out.println("Seleccione un curso por su [indice]: ");
                    imprimirCursos(cursos);
                    icurso = sc.nextInt() - 1;
                    mostrarListaAlumnos(cursos[icurso].getAlumnos());
                    
                    break;
                case 6: 
                    System.out.println("Gracias por utilizar nuestro software");
                    System.exit(0);
                    break;
                default:
                    System.out.println("No es una opcion valida");
                    break;
            }
            
        }while(opcion != 6);    
               
    }
    
    
    
    /*
        Metodo es una pieza de código que se va a ejecutar más de una vez a lo largo de mi 
        programa de software.
        <modificador_acceso> <static?> <retorno (void | tipoDato)> <identificador>(<parametros? (variables)> ){
                <Código de mi logica>
        }

    
    */
    
    
    
    public static void imprimirCursos(Curso cursos[]){
        
        for(int i=0; i<cursos.length; i++){
            if(cursos[i] != null){
                System.out.println("["+(i+1)+"] "+cursos[i].getNombre());
            }
        }
    }
      
    public static void mostrarListaAlumnos(Alumno alumnos[]){
        for(int i=0; i<alumnos.length; i++){
            if(alumnos[i] != null){
                System.out.println("["+(i+1)+"] "+alumnos[i].getNombre());
            }
        }
    }
    
}
