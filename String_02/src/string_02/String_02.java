/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package string_02;

/**
 *
 * @author jorge
 */
public class String_02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String frase = "Thanos: yo soy inevitable. | Tony: y yo soy Iron man..";
        String nombre = new String(); //Instanciar una clase (Objeto)
       
        int vocales = 0;
        
        String fraseSplit[] = frase.split("");
//        System.out.println("Total de fraseSplit: "+fraseSplit.length);
        for(String letra : fraseSplit){
            if(letra.equalsIgnoreCase("a") || letra.equalsIgnoreCase("e") ||
                    letra.equalsIgnoreCase("i") || letra.equalsIgnoreCase("o")
                    || letra.equalsIgnoreCase("u")){
                vocales++;
            }
        }
       
       /* for(char letra : frase.toCharArray()){
            if(letra == 'a' || letra == 'e' || letra == 'i'
                    || letra == 'o' || letra == 'u'
                  || letra == 'A' || letra == 'E' || letra == 'I'
                    || letra == 'O' || letra == 'U'  
                    
                    ){
                vocales++;
            }
        }*/
        
        System.out.println("La frase contiene "+vocales+" vocales.");
        

       /* String frases2[] = frase.split("\\|"); 
        System.out.println(frases2.length);
        System.out.println(frases2[0]);
        System.out.println(frases2[1].trim());*/
        
        /*
            replace(S1,S2);
            reemplaza lo que contenga S1, poniendo lo que tenga
            S2
            Ej. String texto = "Hola mundo mundo mundo";
            String texto2 = texto.replace("mundo","java");
            texto2 -->  "Hola java java java";
       
       
            trim() Borra los espacios en blanco antes y despues de un String
        */
       
        String frases[] = frase.replace("|",":").split(":");
//        System.out.println("Tamaño de frases: "+frases.length);
        System.out.println(frases[0].trim()+" dice: "+frases[1].trim());
        System.out.println(frases[2].trim()+" contesta: "+frases[3].trim());
        
        
        for(String letra : frase.split("")){
            System.out.println(letra);
        }
        
        System.out.println("El tamaño de la frase es: "+frase.length());
        
    }
    
}
