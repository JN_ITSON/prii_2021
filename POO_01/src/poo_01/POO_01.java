/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo_01;

import entidades.Automovil;
import entidades.Avion;

/**
 *
 * @author jorge
 */
public class POO_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Automovil auto = new Automovil();
        Avion avion = new Avion();
        
        //Lote autos 
        Automovil autos[] = new Automovil[3];
          
        for(int i=0; i<autos.length; i++){
            autos[i] = new Automovil();
        }
                
        autos[0].setColor("Rojo");
        autos[1].setColor("Verde");
        autos[2].setColor("Amarillo");
        
        
        for(int i=0; i<autos.length; i++){
            System.out.println("El auto "+(i+1)+" es de color : "+autos[i].getColor());
        }
        
        auto.avanzar();
        auto.detener();
        
        avion.avanzar();
        avion.detener();
      
    }
    
}
