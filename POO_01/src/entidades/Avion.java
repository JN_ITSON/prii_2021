
package entidades;


public class Avion extends Transporte{
    //Atributos
    
    //Constructor
    public Avion(){
        
    }
    
    //Getters And Setters
    
    //Metodos del objeto
    @Override
    public void avanzar(){
        System.out.println("El avión está volando.");
    }
    
    @Override
    public void detener(){
        System.out.println("El avión está aterrizando");
    }
    
    @Override
    public String toString(){
        return "Hola mundo desde el avión";
    }
    
    
}
