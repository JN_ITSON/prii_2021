/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author jorge
 */
public class Transporte {//Automovil - Avión
    //Atributos
    private int pasajeros = 0;
    private int ruedas = 0;
    private String motor = new String();
    private int puertas = 0;
    private int ventanas = 0;
    
    //Constructores
    public Transporte(){
        
    }
    //Getter And Setters
    public int getPasajeros(){
        return this.pasajeros;
    }
    
    public void setPasajeros(int pasajeros){
        this.pasajeros = pasajeros;
    }
    
    public int getRuedas(){
        return this.ruedas;
    }
    
    public void setRuedas(int ruedas){
        this.ruedas = ruedas;
    }
    
    public String getMotor(){
        return this.motor;
    }
    
    public void setMotor(String motor){
        this.motor = motor;
    }
    
    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public int getVentanas() {
        return ventanas;
    }

    public void setVentanas(int ventanas) {
        this.ventanas = ventanas;
    }
       
    
    //Metodos propios del objeto.
    public void avanzar(){
        System.out.println("El transporte está avanzando.");
    }
    
    public void detener(){
        System.out.println("El transporte se detuvo.");
    }
    
}
