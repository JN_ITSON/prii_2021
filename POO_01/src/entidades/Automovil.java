package entidades;


public class Automovil extends Transporte {
    //Atributos
    private String color = new String();
    
    //Constructores
    public Automovil(){
        
    }
    
    //Getters And Setters

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    //Metodos propios del objeto
    public void reversa(){
        System.out.println("El automovil está dando reversa.");
    }
    
    
}
