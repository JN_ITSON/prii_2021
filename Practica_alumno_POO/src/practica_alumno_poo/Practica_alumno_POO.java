/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica_alumno_poo;

import entidades.Alumno;
import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class Practica_alumno_POO {

    /**
     * 
     * Diseñar un programa en Java para registro de cursos
     * Con el siguiente menú
     * 
     * -----[Menú]-----
     * 1.- Agregar Curso (5 Cursos)
     * 2.- Agregar un Alumno al curso (Capacidad 20)
     * 3.- Dar de baja un Alumno
     * 4.- Mostrar la lista de cursos
     * 5.- Mostrar la lista de alumnos de un curso
     * 6.- Salir
     * 
     */
    public static void main(String[] args) {
        
        Alumno lista[] = new Alumno[20];
        int opcion = 0;
        Scanner sc = new Scanner(System.in);
        String menu = "----[Menú]----";
               menu+= "\n1.-Agregar Alumno";
               menu+= "\n2.-Asignar Calificación";               
               menu+= "\n3.-Eliminar Alumno";
               menu+= "\n4.-Mostrar Lista de Alumnos";
               menu+= "\n5.-Salir";
               menu+= "\nDigite su opción: ";
               
               
        do{
            System.out.println(menu);
            opcion = sc.nextInt();
            
            switch(opcion){
                case 1: //Agregar Alumno
                    
                    for(int i=0; i<lista.length; i++){
                        if(lista[i] == null){
                            lista[i] = new Alumno();
                            System.out.println("Digite el nombre del alumno");
                            String nombre = sc.next();
                            lista[i].setNombre(nombre);
                            System.out.println("Se agregó el alumno correctamente");
                            break;
                        }
                        
                        if(i == lista.length -1){
                            System.out.println("Lista está llena");
                        } 
                    }
                    
                    break;
                case 2: //Asignar Calificación
                    for(int i=0; i<lista.length; i++){
                        if(lista[i] != null){
                            System.out.println("["+i+"] - "+lista[i].getNombre());
                        }
                    }
                    
                    System.out.println("Digite el [indice] del alumno: ");
                    int indice = sc.nextInt();
                    System.out.println("Digite la calificación para "+lista[indice].getNombre());
                    int calificacion = sc.nextInt();
                    lista[indice].setCalificacion(calificacion);
                    System.out.println("Se ha registrado la calificación correctamente.");
                    
                    break;
                case 3: //Eliminar un alumno.
                    for(int i=0; i<lista.length; i++){
                        if(lista[i] != null){
                            System.out.println("["+i+"] - "+lista[i].getNombre());
                        }
                    }
                    
                    System.out.println("Digite el [indice] del alumno que desea dar de baja:");
                    int indice_eliminar = sc.nextInt();
                    System.out.println("Se ha eliminado a "+lista[indice_eliminar].getNombre());
                    lista[indice_eliminar] = null;
                    
                    break;
                case 4: //Mostrar lista de alumnos
                      for(int i=0; i<lista.length; i++){
                        if(lista[i] != null){
                            System.out.println("["+i+"] - "+lista[i].getNombre()+" Calif: "+lista[i].getCalificacion());
                        }
                    }
                    
                    break;
                case 5: // Salir
                    System.out.println("Gracias por usar nuestro sistema.");
                    System.exit(0);                    
                    break;
                default:
                    System.out.println("No es una opción valida.");
                    break;
            }
            
        }while(opcion != 5);
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       /*Alumno a1 = new Alumno();
       Alumno a2 = new Alumno("Ivan Ramirez",9);
       
              
       a1.setNombre("Rodolfo Felix");
       a1.setCalificacion(10);
       a2.setCalificacion(10);
       
       System.out.println("Alumno 1: "+a1.getNombre()+" Calif: "+a1.getCalificacion());
       System.out.println("Alumno 2: "+a2.getNombre()+" Calif: "+a2.getCalificacion());
       */
    }
    
}
