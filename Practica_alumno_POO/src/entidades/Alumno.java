package entidades;


public class Alumno {
    //Atributos
    private String nombre = new String();
    private int calificacion = 0;
    
    //Constructores
    public Alumno(){
        
    }
    
    public Alumno(String nombre, int calif){
        this.nombre = nombre;
        this.calificacion = calif;        
    }
    
    
    //Getters y Setters
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }
    
    
    //Metodos de la clase
}
