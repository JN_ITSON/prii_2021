/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1_matriz;

import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class Practica1_matriz {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        int m[][] = new int[3][3];
        int total = 0;
        
        System.out.println(m.length); //el total de filas
        System.out.println(m[0].length); //el total  de columnas
        
        int sumCol[] = new int[m[0].length];
        int sumFil[] = new int[m.length];
        
        for(int i=0; i<m.length; i++){
            for(int j=0; j<m[i].length; j++){
                System.out.println("Introduzca un numero: ");
                m[i][j] = sc.nextInt();
                total += m[i][j]; //Almaceno el total
            }
        }
        
//        int col0=0,col1=0,col2=0;
//        int fil0=0,fil1=0, fil2=0;
        for(int i=0; i<m.length; i++){
            for(int j=0; j<m[i].length; j++){
                sumFil[i] += m[i][j]; //Este incrementa o cambia de posicion cada iteracion de i (fila)
                sumCol[j] += m[i][j]; //Este incrementa cada iteracion de j (columnas)
                
//                if(i==0){
//                    fil0 += m[i][j];
//                }else if(i==1){
//                    fil1 += m[i][j];
//                }else{
//                    fil2 += m[i][j];
//                }
                
                
//                if(j==0){
//                    col0+= m[i][j];
//                }else if(j==1){
//                    col1+= m[i][j];
//                }else{
//                    col2 += m[i][j];
//                }
            }
        }
        
        
        for(int i=0; i<m.length; i++){
            for(int j=0; j<m[i].length; j++){
                System.out.print("["+m[i][j]+"]");
            }
            System.out.println("= "+sumFil[i]);
        }
        
      
        for(int i=0; i<sumCol.length; i++){
            System.out.print("{"+sumCol[i]+"}");
        }
        System.out.println("= "+total);
        
    }
   
}
