/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1_arreglos;

import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class Practica1_arreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    
      Scanner sc = new Scanner(System.in);
        
    /*1.-Declare un arreglo en el cual introduzca por teclado 
        el nombre de 10 personas. Una vez lleno el arreglo muestrelo 
        por pantalla.
    */
    String nombres[] = new String[10];
    
    for(int i=0; i<nombres.length; i++){
        System.out.println("Introduzca el nombre de una persona.");
        nombres[i] = sc.nextLine();
    }
    
    for(String nombre : nombres){//for each
        System.out.println(nombre);
    }

    /*2.-Guarda en un arreglo 30 números enteros que se leen por teclado. 
       Recorre el arreglo y calcula cuántos números son positivos, 
       cuántos negativos y cuántos ceros.*/
    int num[]= new int[30];
    int positivos=0;
    int negativos=0;
    int ceros=0;
    
    for(int i=0; i<num.length; i++){
        System.out.println("Introduzca un numero: ");
        num[i] = sc.nextInt();
        
        if(num[i]>0){//Es positivo
            positivos++;
        }else if(num[i]<0){//Es negativo
            negativos++;
        }else{
            ceros++;
        }
    }

    System.out.println("Total positivos: "+positivos);
    System.out.println("Total negativos: "+negativos);
    System.out.println("Total ceros: "+ceros);

    /*3.-Dentro de un arreglo guarde la altura de 8 personas. 
    Calcule el promedio de todas las alturas.*/
    double altura[] = {1.65,1.70,1.50,2.03,1.78,1.68,1.66,1.83};
    double promedio = 0;
    double suma = 0;
    for(double alt : altura){
        suma += alt;
//        promedio +=alt;
    }
    
    promedio= suma / altura.length;
//    promedio/= altura.length;
//    promedio = promedio/altura.length;

    System.out.println("La estatura media de "+altura.length
            +" personas es: "+promedio+" metros");

    /*4.-Por medio de un arreglo, almacene el peso de 15 personas. 
    Determine cual es el peso menor, el peso mayor y el peso promedio.*/
    double pesoMenor=0;
    double pesoMayor=0;
    double prom=0;
    double peso[] = new double[15];
    peso[0]= 70.4;
    peso[1]= 74.6;
    peso[2]= 85.8;
    peso[3]= 90.4;
    peso[4]= 110.5;
    
    peso[5]= 80.6;
    peso[6]= 65.6;
    peso[7]= 30.8;
    peso[8]= 50.4;
    peso[9]= 130.25;
    
    peso[10]= 170.4;
    peso[11]= 47.5;
    peso[12]= 99.8;
    peso[13]= 79.4;
    peso[14]= 40.5;
    
    pesoMayor = peso[0];
    pesoMenor = peso[0];
    for(int i=0; i<peso.length; i++){
        prom+= peso[i];
        
        if(peso[i]>pesoMayor){
            pesoMayor = peso[i];
        }else if(peso[i]<pesoMenor){
            pesoMenor = peso[i];
        }
    }
    
    promedio /= peso.length;
    System.out.println("El peso mayor: "+pesoMayor);
    System.out.println("El peso menor: "+pesoMenor);
    System.out.println("El promedio: "+prom);

    /*5.-Dentro de una matriz de 3 x 4 almacene números ingresados 
    por teclado. Una vez llena la matriz deberá mostrarla por consola 
    como el siguiente ejemplo:
    muestre en pantalla la suma de todos sus valores.*/
    int filas = 3;
    int columnas = 4;
    int matriz[][] = new int[filas][columnas];
    int sum = 0;

    for(int fila=0; fila<filas; fila++){//Introduzco valores
        for(int columna=0; columna<columnas; columna++){
            System.out.println("Introduzca un numero: ");
            matriz[fila][columna] = sc.nextInt();
            sum += matriz[fila][columna];
        }
    } 
    
    for(int i=0; i<filas; i++){ //Imprimo mi matriz
        for(int j=0; j<columnas; j++){
            System.out.print("["+matriz[i][j]+"]");
        }
        System.out.println("");
    }
    
    System.out.println("");
    System.out.println("La suma de los valores de la matriz es: "+sum);
}
    
}
