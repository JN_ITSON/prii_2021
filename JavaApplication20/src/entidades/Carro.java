/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author jorge
 */
public class Carro {
    // ==== ATRIBUTOS ======
    private String marca = new String();
    private String modelo = new String();
    private String motor  = new String();
    private int noPuertas = 0;
    private String color = new String();
    private double peso = 0.0;
    private int numeroSerie = 0;
    
    // ==== Constructores ======
    public Carro(){ //Constructor vacio
        
    }
    
    public Carro(String modelo, String marca, String motor, int noPuertas, String color , double peso){
        this.modelo = modelo;
        this.marca = marca;
        this.motor = motor;
        this.noPuertas = noPuertas;
        this.color = color;
        this.peso = peso;
    }
    
    
    // ==== GETTERS && SETTERS (Encapsulamiento) ==========
    public void setMarca(String marca){
        this.marca = marca;
    }
    
    public String getMarca(){
        return this.marca;
    }
    
    public void setNumeroSerie(int serie){
        this.numeroSerie = serie;
    }
    
    
    // ==== Metodos de la clase ================
    public String imprimir(){
        String carro = " Marca:  "+this.marca;
               carro += "\n Modelo: "+this.modelo;
               carro += "\n Motor: "+this.motor;
               carro += "\n Numero de Puertas : "+this.noPuertas;
               carro += "\n Color: "+this.color;
               carro += "\n Peso: "+this.peso;
               carro += "\n Serie: "+this.numeroSerie;
        
        return carro;
    }
}
