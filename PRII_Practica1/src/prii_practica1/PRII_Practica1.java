/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prii_practica1;

/**
 *
 * @author jorge
 */
public class PRII_Practica1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nombres[] = new String[20];
        
        nombres[0] = "Paez";
        nombres[1] = "Salvador";
        nombres[2] = "Editado";
        nombres[3] = "Verduzco";
        nombres[4] = "Tejada";
        nombres[5] = "Nuevo";
        nombres[6] = "Martinez";
        nombres[7] = "Felix";
        nombres[8] = "Fierros";
        nombres[9] = "1";
        nombres[10] = "2";
        nombres[11] = "3";
        nombres[12] = "4";
        nombres[13] = "5";
        nombres[14] = "6";
        nombres[15] = "7";
        nombres[16] = "8";
        nombres[17] = "9";
        nombres[18] = "10";
        nombres[19] = "11";
        
        //Loops
        System.out.println("INICIO DE FOR");
        for(int i=0; i<nombres.length; i++){
            System.out.println(nombres[i]);
            
            //Bifurcacion (IF-Else-Else if   =  Switch)
            if(nombres[i] == "Nuevo"){
                System.out.println("Se ha agregado un nuevo Elemento");
            }else if(nombres[i] == "Editado"){
                System.out.println("Se ha modificado un Elemento");
            }
            
            switch(nombres[i]){
                case "Nuevo": 
                        System.out.println("Se ha agregado un nuevo Elemento"); 
                        break;
                        
                case "Editado": 
                    System.out.println("Se ha modificado un Elemento");  
                    break;
            }
            
        }
        
        
        System.out.println("INICIO DE WHILE");
        int i = 0;
        while(i<nombres.length){
            System.out.println(nombres[i]);
            
            //Bifurcacion (IF-Else-Else if   =  Switch)
            if(nombres[i] == "Nuevo"){
                System.out.println("Se ha agregado un nuevo Elemento");
            }else if(nombres[i] == "Editado"){
                System.out.println("Se ha modificado un Elemento");
            }
            
            switch(nombres[i]){
                case "Nuevo": 
                        System.out.println("Se ha agregado un nuevo Elemento"); 
                        break;
                        
                case "Editado": 
                    System.out.println("Se ha modificado un Elemento");  
                    break;
            }
            i++;
        }
        
        System.out.println("INICIO DE DO WHILE");
        i=0;
        do{
            System.out.println(nombres[i]);
            
            //Bifurcacion (IF-Else-Else if   =  Switch)
            if(nombres[i] == "Nuevo"){
                System.out.println("Se ha agregado un nuevo Elemento");
            }else if(nombres[i] == "Editado"){
                System.out.println("Se ha modificado un Elemento");
            }
            
            switch(nombres[i]){
                case "Nuevo": 
                        System.out.println("Se ha agregado un nuevo Elemento"); 
                        break;
                        
                case "Editado": 
                    System.out.println("Se ha modificado un Elemento");  
                    break;
            }
            i++;
        }while(i<nombres.length);
        
        
        //foreach
        System.out.println("INICIO DE FOREACH");
        for(String nombre : nombres){
            System.out.println(nombre);
            
            //Bifurcacion (IF-Else-Else if   =  Switch)
            if(nombre == "Nuevo"){
                System.out.println("Se ha agregado un nuevo Elemento");
            }else if(nombre == "Editado"){
                System.out.println("Se ha modificado un Elemento");
            }
            
            switch(nombre){
                case "Nuevo": 
                        System.out.println("Se ha agregado un nuevo Elemento"); 
                        break;
                        
                case "Editado": 
                    System.out.println("Se ha modificado un Elemento");  
                    break;
            }
        }
        
    }
    
}
