/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos_01;

/**
 *
 * @author jorge
 */
public class Arreglos_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /*
        Conjunto de datos de un mismo tipo como:
        String
        Object
        Clase
        int
        double
        boolean
        ...
           
        
        */
        String cadena = "1234jihsvmkl@";
        
        String nombres[] = new String[5];
        nombres[0] = "Fierros";
        nombres[1] = "Cañedo";
        nombres[2] = "Ruiz";
        nombres[3] = "Felix";
        nombres[4] = "Nohemi";
     //Tipo identificador  = Inisalizacion
     //   int edades[]       = new int[5]; 
//     int edad[] = new int[6]; 
        int edades[] = {22,23,24,25,26}; 
        System.out.println("El arreglo Edades tiene "+edades.length+" espacios ");
     
        /*
            
            int counter = 0;
        
            counter++ Incrementado en uno la variable en el siguiente turno
            counter vale 1
        
            ++counter Incrementa en uno la variable - counter vale 1
        
            counter = 0;
            counter-- Decrementa en uno la variable en el siguiente turno 0
            counter vale -1
            --counter  Decrementa en este momento counter vale -1
        */
        
//        int counter = 0;
//        System.out.println("Counter++ "+ counter++);
//        counter = 0;
//        System.out.println("++Counter "+ (++counter));
        
        
//        for(int i=0; i<nombres.length; i++){
//            System.out.println("Valor de nombres en i="+i+" es: "+nombres[i]);
//        }
        
        String busqueda = "Nohemi";
        int indice = -1;
        for(int i=0; i<nombres.length; i++){
            if(nombres[i] == busqueda){
                indice = i;
                break;
            }
            System.out.println("indice i= "+i);
        }
        System.out.println("El valor de indice = "+indice);
        System.out.println("La edad de "+busqueda+" es "+edades[indice]+" años");
    }
    
}
