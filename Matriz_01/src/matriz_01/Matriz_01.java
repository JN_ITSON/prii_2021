/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matriz_01;

import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class Matriz_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int filas = 5;
        int columnas = 3;
        //Scanner me ayuda a obtener datos a traves del teclado
        Scanner sc = new Scanner(System.in);
        int mayor;
        int menor;
        int filaMayor= 0;
        int filaMenor= 0;
        int colMayor= 0;
        int colMenor= 0;
        
        int lksdhnjhcs = 0;
        
        int[][] num = new int[filas][columnas];
        
        //Capturar datos
        System.out.println("Captura los datos de la matriz: ");
        for(int i=0; i<filas; i++){//Este for recorre las filas
            for(int j=0; j<columnas; j++){//Este for recorre las columnas
                System.out.println("num["+i+"]["+j+"]");//Concatenar String
                num[i][j] = sc.nextInt();
            }
        }
        
//        i=0;
//        j=2;
//        valoresFila = valoresFila+ "["+num[i][j]+"]";
//        
//      1er vuelta  "[1]"
//      2da vuelta  "[1][2]"
//      3er vuelta  "[1][2][3]"
//      4ta vuelta ""
//      5ta vuelta  "[4]"
//      6da vuelta  "[4][5]"
//      7er vuelta  "[4][5][6]"
        for(int i=0; i<filas; i++){
            String valoresFila = "";
            for(int j=0; j<columnas;j++){
                valoresFila += "["+num[i][j]+"]";
            }
            System.out.println(valoresFila);
        }
        
        mayor = num[0][0];
        menor = num[0][0];
//        mayor = 8;    menor = -1;
//        [1][6][8]
//        [0][-1][3]
//        i= 1;   j=2;
        for(int i=0; i<filas; i++){
            for(int j=0; j<columnas; j++){
                
                if(num[i][j] < menor){//obtenemos el numero menor
                    menor = num[i][j]; //Definimos el valor menor
                    filaMenor = i; //Definimos la fila menor
                    colMenor = j; //Definimos la columna menor
                    
                }else if(num[i][j] > mayor){//obtenemos el numero mayor
                    mayor = num[i][j];//Definimos el valor mayor
                    filaMayor = i; //Definimos la fila mayor
                    colMayor = j; //Definimos la columna mayor
                }
                
            }
        }
        
        System.out.print("El numero menor es: "+menor);
        System.out.print(" se encuentra en fila ["+filaMenor+"]");
        System.out.println(" y columna["+colMenor+"]");
        
        
        System.out.print("El numero mayor es: "+mayor);
        System.out.print(" se encuentra en fila ["+filaMayor+"]");
        System.out.println(" y columna["+colMayor+"]");
        
    }
    
}
